/*
 Copyright (C) 2013 <ale@incal.net>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/

#include <appname.h>
#include "analyzer.h"
#include "model.h"
#include "imms-c.h"

// Define it for all users of the library.
const string AppName = "imms";

imms_analyzer_t imms_stream_analyzer_new() {
  return (imms_analyzer_t)new StreamAnalyzer();
}

void imms_stream_analyzer_process(imms_analyzer_t aptr, char *data, int n) {
  StreamAnalyzer *sa = reinterpret_cast<StreamAnalyzer*>(aptr);
  sa->process(data, n);
}

imms_features_t imms_stream_analyzer_get_result(imms_analyzer_t aptr) {
  StreamAnalyzer *sa = reinterpret_cast<StreamAnalyzer*>(aptr);
  return (imms_features_t)sa->get_result();
}

void imms_stream_analyzer_free(imms_analyzer_t aptr) {
  StreamAnalyzer *sa = reinterpret_cast<StreamAnalyzer*>(aptr);
  delete sa;
}

imms_features_t imms_features_from_data(char *buf, int n) {
  return (imms_features_t)(new Features(buf, n));
}

int imms_features_data_size() {
  return Features::SerializedSize;
}

void imms_features_data(imms_features_t ptr_, char *buf, int n) {
  Features *f = reinterpret_cast<Features*>(ptr_);
  f->serialize(buf, n);
}

void imms_features_free(imms_features_t ptr_) {
  Features *f = reinterpret_cast<Features*>(ptr_);
  delete f;
}

imms_similarity_model_t imms_similarity_model_new() {
  SVMSimilarityModel *model = new SVMSimilarityModel();
  return (imms_similarity_model_t)model;
}

float imms_similarity_model_evaluate(imms_similarity_model_t ptr_,
                                     imms_features_t f1_,
                                     imms_features_t f2_) {
  SVMSimilarityModel *model = reinterpret_cast<SVMSimilarityModel*>(ptr_);
  Features *f1 = reinterpret_cast<Features*>(f1_);
  Features *f2 = reinterpret_cast<Features*>(f2_);

  return model->evaluate(f1->mfcc(), f1->beats(),
                         f2->mfcc(), f2->beats());
}

void imms_similarity_model_free(imms_similarity_model_t ptr_) {
  SVMSimilarityModel *model = reinterpret_cast<SVMSimilarityModel*>(ptr_);
  delete model;
}

