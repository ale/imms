
TORCH_LIB = $(top_builddir)/ext/torch3/libtorch.a
IMMSCORE_LIB = $(top_builddir)/immscore/libimmscore.a
ANALYZER_LIB = $(top_builddir)/analyzer/libimmsanalyzer.a
MODEL_LIB = $(top_builddir)/model/libimmsmodel.a

INCS = \
	-I$(top_srcdir)/ext/torch3/core \
	-I$(top_srcdir)/ext/torch3/distributions \
	-I$(top_srcdir)/ext/torch3/gradients \
	-I$(top_srcdir)/ext/torch3/kernels \
	-I$(top_srcdir)/immscore \
	-I$(top_srcdir)/analyzer \
	-I$(top_srcdir)/model

AM_CPPFLAGS = @CPPFLAGS@ @XCPPFLAGS@ -Wall -fPIC -D_REENTRANT $(INCS)
AM_CXXFLAGS = @CXXFLAGS@ -fno-rtti

AM_LDFLAGS = @LIBS@

