package imms

// #cgo CFLAGS: -I/usr/local/include
// #cgo LDFLAGS: -L/usr/local/lib -limms-c
// #include <imms-c.h>
import "C"
import "unsafe"

type StreamAnalyzer struct {
	ptr C.imms_analyzer_t
}

func NewStreamAnalyzer() *StreamAnalyzer {
	return &StreamAnalyzer{C.imms_stream_analyzer_new()}
}

func (s *StreamAnalyzer) Process(data []byte) {
	dataPtr := (*C.char)(unsafe.Pointer(&data[0]))
	C.imms_stream_analyzer_process(s.ptr, dataPtr, C.int(len(data)))
}

func (s *StreamAnalyzer) GetResult() *Features {
	return &Features{C.imms_stream_analyzer_get_result(s.ptr)}
}

func (s *StreamAnalyzer) Close() {
	C.imms_stream_analyzer_free(s.ptr)
}

type Features struct {
	ptr C.imms_features_t
}

func NewFeaturesFromData(data []byte) *Features {
	bufPtr := (*C.char)(unsafe.Pointer(&data[0]))
	return &Features{C.imms_features_from_data(bufPtr, C.int(len(data)))}
}

func (f *Features) Data() []byte {
	n := C.imms_features_data_size()
	buf := make([]byte, n)
	bufPtr := (*C.char)(unsafe.Pointer(&buf[0]))
	C.imms_features_data(f.ptr, bufPtr, n)
	return buf
}

func (f *Features) Close() {
	C.imms_features_free(f.ptr)
}

type SimilarityModel struct {
	ptr C.imms_similarity_model_t
}

func NewSimilarityModel() *SimilarityModel {
	return &SimilarityModel{C.imms_similarity_model_new()}
}

func (m *SimilarityModel) Eval(a, b *Features) float32 {
	score := C.imms_similarity_model_evaluate(m.ptr, a.ptr, b.ptr)
	return float32(score)
}

func (m *SimilarityModel) Close() {
	C.imms_similarity_model_free(m.ptr)
}
