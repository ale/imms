#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "imms-c.h"

#define min(a, b) (((a) < (b)) ? (a) : (b))

FILE *convert_to_22050khz(char *path) {
  char buf[1024];
  //sprintf(buf, "avconv -i \"%s\" -f s16le -ar 22050 -ac 1 -ss 60 -t 60 -", path);
  sprintf(buf, "avconv -i \"%s\" -f u16le -ar 22050 -ac 1 -", path);
  return popen(buf, "r");
}

imms_features_t analyze_file(char *path) {
  FILE *fd = convert_to_22050khz(path);
  if (!fd) {
    return NULL;
  }

  imms_analyzer_t analyzer = imms_stream_analyzer_new(); 

  int bufsz = 4096;
  char *buf = (char *)malloc(bufsz);
  int n = 0;
  while (!feof(fd)) {
    int l = fread(buf, 1, bufsz, fd);
    if (l <= 0) {
      break;
    }
    n += l;
    imms_stream_analyzer_process(analyzer, buf, l);
  }
  fclose(fd);

  // Analyze the resulting data.
  imms_features_t f = imms_stream_analyzer_get_result(analyzer);
  imms_stream_analyzer_free(analyzer);
  free(buf);

  return f;
}

void compare_many(char **paths, int n) {
  int i, j, k;
  imms_features_t *feats = (imms_features_t *)malloc(sizeof(imms_features_t) * n);
  for (i = 0, k = 0; i < n; i++) {
    printf("extracting features from %s\n", paths[i]);
    imms_features_t f = analyze_file(paths[i]);
    if (f == NULL) {
      printf("NULL features for %s\n", paths[i]);
    } else {
      feats[k++] = f;
    }
  }
  n = k;

  imms_similarity_model_t model = imms_similarity_model_new();

  int *results = (int *)calloc(sizeof(int), n * n);
  printf("comparing...\n");
  for (i = 0; i < n; i++) {
    for (j = 0; j <= i; j++) {
      float value = imms_similarity_model_evaluate(model, feats[i], feats[j]);
      results[i * n + j] = (int)(100 * value);
    }
  }

  imms_similarity_model_free(model);

  printf("\nresults:\n");
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      printf(" %6d", results[i * n + j]);
    }
    printf("\n");
  }

}

int main(int argc, char **argv) {
  compare_many(argv + 1, argc - 1);
}
