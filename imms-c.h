/*
 Copyright (C) 2013 <ale@incal.net>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef __imms_cint_H
#define __imms_cint_H

#ifdef __cplusplus
extern "C" {
#endif

// Opaque types.
typedef void *imms_analyzer_t;
typedef void *imms_features_t;
typedef void *imms_similarity_model_t;

// C wrappers for the analysis/model API.

// A StreamAnalyzer analyzes the input stream (passed in buffers of
// arbitrary size by the caller) and returns feature data.
imms_analyzer_t imms_stream_analyzer_new();

void imms_stream_analyzer_process(imms_analyzer_t, char *, int);

imms_features_t imms_stream_analyzer_get_result(imms_analyzer_t);

void imms_stream_analyzer_free(imms_analyzer_t);

// Deserialize feature data.
imms_features_t imms_features_from_data(char *, int);

// Returns the size of the serialized feature data, in bytes.
int imms_features_data_size();

// Serialize feature data into the provided buffer.
void imms_features_data(imms_features_t, char *, int);

// Free resources associated with feature data.
void imms_features_free(imms_features_t);

// Create a new similarity model.
imms_similarity_model_t imms_similarity_model_new();

// Evaluate the similarity between two feature sets.
float imms_similarity_model_evaluate(imms_similarity_model_t, imms_features_t, imms_features_t);

// Free resources associated with the similarity model.
void imms_similarity_model_free(imms_similarity_model_t);

#ifdef __cplusplus
}
#endif

#endif
