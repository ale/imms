/*
 IMMS: Intelligent Multimedia Management System
 Copyright (C) 2001-2009 Michael Grigoriev
 Copyright (C) 2013 <ale@incal.net>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#include <errno.h>
#include <iostream>
#include <stdint.h>
#include <stdio.h>
#include <string>
#include <string.h>
#include <unistd.h>

#include <immsutil.h>

#include "analyzer.h"
#include "strmanip.h"
#include "mfcckeeper.h"
#include "beatkeeper.h"

using std::cout;
using std::cerr;
using std::endl;

#define STATE_PREFILL    0
#define STATE_RUN        1

StreamAnalyzer::StreamAnalyzer()
  : hanwin(WINDOWSIZE),
    state(STATE_PREFILL),
    frames(0),
    buf_pos(0),
    outdata(NUMFREQS)
{}

void StreamAnalyzer::process(char *data, int n) {
  char *buf_head = (char *)(indata + OVERLAP);
  while (n > 0) {
    int sz = sizeof(sample_t) * READSIZE - buf_pos;
    if (n < sz)
       sz = n;
    memcpy(buf_head + buf_pos, data, sz);
    n -= sz;
    data += sz;
    buf_pos += sz;
    if (buf_pos == sizeof(sample_t) * READSIZE) {
      buf_pos = 0;
      process_buffer();
    }
  }
}

void StreamAnalyzer::process_buffer() {
  switch(state) {
  case STATE_PREFILL:
    state = STATE_RUN;
    break;
  case STATE_RUN:
    process_buffer_internal();
  }

  // shift the read data
  memmove(indata, indata + READSIZE, OVERLAP * sizeof(sample_t));
}

void StreamAnalyzer::process_buffer_internal() {
  if (++frames > MAXFRAMES)
     return;

  // calculate MFCCs:
  for (int i = 0; i < WINDOWSIZE; ++i)
     pcmfft.input()[i] = (double)indata[i];
  
  // window the data
  hanwin.apply(pcmfft.input(), WINDOWSIZE);

  // fft to get the spectrum
  pcmfft.execute();

  // calculate the power spectrum
  for (int i = 0; i < NUMFREQS; ++i)
     outdata[i] = pow(pcmfft.output()[i][0], 2) +
        pow(pcmfft.output()[i][1], 2);

  // apply mel filter bank
  vector<double> melfreqs;
  mfbank.apply(outdata, melfreqs);

  beatkeeper.process(melfreqs);

  // compute log energy
  for (int i = 0; i < NUMMEL; ++i)
     melfreqs[i] = log(melfreqs[i]);
  
  // another fft to get the MFCCs
  specfft.apply(melfreqs);
  
  // discard the first mfcc
  float cepstrum[NUMCEPSTR];
  for (int i = 1; i <= NUMCEPSTR; ++i)
     cepstrum[i - 1] = specfft.output()[i][0];
  
  mfcckeeper.process(cepstrum);
}

Features *StreamAnalyzer::get_result() {
  if (frames < 100)
     return NULL;

  mfcckeeper.finalize();
  beatkeeper.finalize();

  return new Features(mfcckeeper.get_result(), beatkeeper.get_result());
}
