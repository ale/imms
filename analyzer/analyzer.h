/*
 IMMS: Intelligent Multimedia Management System
 Copyright (C) 2001-2009 Michael Grigoriev

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef __ANALYZER_H
#define __ANALYZER_H

#include <string>
#include <math.h>
#include <stdint.h>

#include "analyzer-params.h"
#include "melfilter.h"
#include "imms-features.h"
#include "fftprovider.h"
#include "hanning.h"

typedef uint16_t sample_t;

// Calculate acoustic stats for a song.
//
// Analyzer calculates the Beats Per Minute (BPM) and 
// Mel-frequency cepstral coefficients (MFCC) for a song. These stats are used 
// by IMMS to boost/penalize song transitions for songs that have 
// similar/dissimilar acoustic characteristics - i.e. Analyzer helps IMMS 
// match the 'mood'/theme of the next song to the previous one.
//
// BPM is a measure of how "fast" a song is; 
// this is a valuable signal as slow and fast songs generally don't mix.
// MFCC is meant to capture the type of the song; 
// i.e. what instruments are used, type of vocals, etc.
//
// As of IMMS 1.2 Analyzer is a separate application, and called as needed.
// Analyzer is an optional component; if not used IMMS will simply use its 
// other sources to determine the next song.
class StreamAnalyzer
{
public:
   StreamAnalyzer();
   void process(char *data, int n);
   Features *get_result();
protected:
   void process_buffer();
   void process_buffer_internal();

   FFTWisdom wisdom;
   FFTProvider<WINDOWSIZE> pcmfft;
   FFTProvider<NUMMEL> specfft;
   MelFilterBank mfbank;
   HanningWindow hanwin;

   int state;
   size_t frames;
   int buf_pos;
   sample_t indata[WINDOWSIZE];
   vector<double> outdata;
   MFCCKeeper mfcckeeper;
   BeatManager beatkeeper;
};

#endif
