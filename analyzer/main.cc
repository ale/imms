/* Test program for StreamAnalyzer. */

#include <iostream>
#include <sstream>
#include <string>

#include <getopt.h>
#include <unistd.h>

#include <immsutil.h>
#include <appname.h>
#include <base64.h>

#include "analyzer.h"

using std::cout;
using std::cerr;
using std::endl;

const string AppName = ANALYZER_APP;


std::string serialize_beats(const float *b) {
  std::ostringstream o;
  o.write((char *)b, BeatManager::ResultSize);
  return o.str();
}

void dump_features_json(Features *f) {
    // Output a JSON array with the result (binary data is encoded
    // using base64).
  std::string mfccbuf = f->mfcc().serialize(),
     beatsbuf = serialize_beats(f->beats());

    cout << "{" << endl
         << "  mfcc: \""
         << base64::base64_encode((unsigned char *)mfccbuf.data(), mfccbuf.size())
         << "\"," << endl
         << "  beats: \""
         << base64::base64_encode((unsigned char *)beatsbuf.data(), beatsbuf.size())
         << "\"" << endl
         << "}" << endl;
}

void dump_features_raw(Features *f, int do_base64) {
  int n = Features::SerializedSize;
  char buf[n];
  f->serialize(buf, n);

  if (do_base64) {
    cout << base64::base64_encode((unsigned char *)buf, n)
         << endl;
  } else {
    write(1, buf, n);
  }
}

Features *analyze(FILE *stream) {
  int n;
  char buf[1024];
  StreamAnalyzer analyzer;
  
  while ((n = fread(buf, 1, sizeof(buf), stream)) > 0) {
    analyzer.process(buf, n);
  }
  return analyzer.get_result();
}

int main(int argc, char *argv[])
{
  int opt;
  int output_base64 = 0;
  int output_json = 0;

  while ((opt = getopt(argc, argv, "bj")) > 0) {
    switch (opt) {
    case 'b':
      output_base64 = 1;
      break;
    case 'j':
      output_json = 1;
      break;
    }
  }

  if (optind < argc) {
    cout << "usage: analyzer [-b|-j]" << endl
         << "File data must be passed on standard input as:" << endl
         << "  raw PCM, 16-bit unsigned, mono, " << SAMPLERATE << " Hz"
         << endl << endl
         << "The output vector will be dumped on standard output." << endl
         << "With the -b option, the output will be base64-encoded." << endl
         << "With the -j option, the output will be a JSON object." << endl
         << endl;
    return 1;
  }

  Features *f = analyze(stdin);
  if (!f) {
    LOG(ERROR) << "Could not process input data." << endl;
  } else {
    if (output_json == 1) {
      dump_features_json(f);
    } else {
      dump_features_raw(f, output_base64);
    }
  }

  return 0;
}
