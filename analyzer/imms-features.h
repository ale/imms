/*
 Copyright (C) 2013 <ale@incal.net>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
#ifndef __imms_features_H
#define __imms_features_H

#include <memory>
#include <string.h>

#include "beatkeeper.h"
#include "mfcckeeper.h"

class Features {
public:
   static const int MfccSerializedSize = MixtureModel::SerializedSize;
   static const int BeatsSerializedSize = BEATSSIZE * sizeof(float);
   static const int SerializedSize = MfccSerializedSize + BeatsSerializedSize;

   Features() {}

   Features(char *buf, int n)
     : mm_((float*)buf) {
     memcpy(beats_, buf + MfccSerializedSize, BeatsSerializedSize);
   }

   Features(const MixtureModel& mm, float *beats)
     : mm_(mm) {
     memcpy(beats_, beats, BeatsSerializedSize);
   }

   const MixtureModel& mfcc() const {
     return mm_;
   }

   const float *beats() const {
     return beats_;
   }

   float *beats() {
     return beats_;
   }

   int serialize(char *buf, int n) {
     if (n < SerializedSize)
       return -1;
     std::string mmbuf = mm_.serialize();
     memcpy(buf, mmbuf.data(), MfccSerializedSize);
     memcpy(buf + MfccSerializedSize, beats_, BeatsSerializedSize);
     return 0;
   }

private:
   MixtureModel mm_;
   float beats_[BEATSSIZE];
};

#endif
